React Local IM Client
===========================

Instant Messaging server and client implementation built using:

* node.js
* socket.io
* React
* Redux
* material-ui
* CSS Modules

